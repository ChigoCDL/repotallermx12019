var clientesObtenidos;
function getCustomers(){
  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if (this.readyState==4&&this.status==200){
      console.log(request.responseText);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }
  request.open("GET",url,true);
  request.send();
}

function procesarClientes(){
  var JSONClientes=JSON.parse(clientesObtenidos);
  var divTabla = document.getElementById("tablaClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");
  tabla.classList.add("table");
  tabla.classList.add("table-striped");
  for (var i=0;i<JSONClientes.value.length;i++){
    //console.log(JSONProductos.value[i].ProductName);
    var nuevaFila = document.createElement("tr");
    var colNombre = document.createElement("td");
    colNombre.innerText = JSONClientes.value[i].ContactName;
    var colCiudad = document.createElement("td");
    colCiudad.innerText = JSONClientes.value[i].City;
    var colPais = document.createElement("td");
    var img = document.createElement("img");
    img.src="https://www.countries-ofthe-world.com/flags-normal/flag-of-"+(JSONClientes.value[i].Country=="UK"?"United-Kingdom":JSONClientes.value[i].Country)+".png";
    img.classList.add("flag");
    //img.height=42;
    //img.width=70;
    colPais.appendChild(img);
    //colPais.innerText = JSONClientes.value[i].Country;

    nuevaFila.appendChild(colNombre);
    nuevaFila.appendChild(colCiudad);
    nuevaFila.appendChild(colPais);
    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
