var productosObtenidos;
function getProductos(){
  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if (this.readyState==4&&this.status==200){
      console.log(request.responseText);
      productosObtenidos = request.responseText;
      procesarProductos();
    }
  }
  request.open("GET",url,true);
  request.send();
}
function procesarProductos(){
  var JSONProductos=JSON.parse(productosObtenidos);
  var divTabla = document.getElementById("tablaProductos");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");
  tabla.classList.add("table");
  tabla.classList.add("table-striped");
  for (var i=0;i<JSONProductos.value.length;i++){
    //console.log(JSONProductos.value[i].ProductName);
    var nuevaFila = document.createElement("tr");
    var colNombre = document.createElement("td");
    colNombre.innerText = JSONProductos.value[i].ProductName;
    var colPrecio = document.createElement("td");
    colPrecio.innerText = JSONProductos.value[i].UnitPrice;
    var colStock = document.createElement("td");
    colStock.innerText = JSONProductos.value[i].UnitsInStock;

    nuevaFila.appendChild(colNombre);
    nuevaFila.appendChild(colPrecio);
    nuevaFila.appendChild(colStock);
    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
